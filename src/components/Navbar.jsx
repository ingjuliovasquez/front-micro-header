import React from 'react'
import Link from './Link'

export default function Navbar() {
    return (
        <div className='w-full bg-white flex fixed top-0 z-50'>
            <div className="container mx-auto flex items-center justify-between">
                <h2 className='text-2xl font-bold py-5 text-blue-600' >Dentistalia</h2>
                <div className="flex items-center gap-5 py-1">
                    <Link href="/">Home</Link>
                    <Link href="/login">login</Link>
                </div>
            </div>
        </div>
    )
}
