import { Button } from 'antd'

export default function Link({ href, children }) {
    return <Button href={href} type="text" >
        {children}
    </Button>
}